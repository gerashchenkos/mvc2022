<?php

error_reporting(E_ALL);

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "vendor" . DIRECTORY_SEPARATOR . "autoload.php";

//define('ENCRYPT_KEY', file_get_contents(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "key.txt"));

session_start();

use Shop\Routes\Route;

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__FILE__, 2));
$dotenv->load();

$_ENV['ENCRYPT_KEY'] = file_get_contents(dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "key.txt");


Route::init();
Route::run($_REQUEST['path'] ?? '/');
