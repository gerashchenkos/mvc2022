<main>

    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Cart Page</h1>
                <p>
                    <a href="#" class="btn btn-secondary my-2">Products</a>
                    <a href="#" class="btn btn-primary my-2">Cart</a>
                </p>
            </div>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">

            <form method="POST" action="">
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                    <?php
                    foreach ($products as $product): ?>
                        <div class="col">
                            <div class="card shadow-sm">
                                <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                                     xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail"
                                     preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title>
                                    <rect width="100%" height="100%" fill="#55595c"></rect>
                                    <text x="50%" y="50%" fill="#eceeef" dy=".3em"><?php
                                        echo $product->name; ?></text>
                                </svg>

                                <div class="card-body">
                                    <p class="card-text float-right"><?php
                                        echo $product->name; ?>
                                        <small class="text-muted">Quantity: <?php
                                            echo $product->quantity; ?></small>
                                    </p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <small class="text-muted"><?php
                                                echo ucfirst($product->category->name); ?></small>
                                        </div>
                                        <small class="text-muted">$<?php
                                            echo $product->price; ?></small>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php
                    endforeach; ?>
                </div>
                <br>
                <div class="row justify-content-center">
                    <div class="col-2">
                        <button type="submit" class="btn btn-primary">Order</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</main>
</body>
</html>