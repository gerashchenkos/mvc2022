<div class="container-fluid">
    <div class="row justify-content-md-center">
        <div class="col-6">
        <form action="/user/post-login" method="post">
            <div class="container">

                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter Email" value="<?php
                    echo $email ?? ''; ?>" name="email" required>
                </div>
                <div class="mb-3">
                    <label for="psw" class="form-label">Password</label>
                    <input type="password" class="form-control" id="psw" placeholder="Enter Password" name="psw"
                           required>
                </div>

                <?php
                if (!empty($isError)): ?>
                    <div class="alert alert-danger" role="alert">
                        Credentials are invalid
                    </div>
                <?php
                endif; ?>

                <div class="mb-3 form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember">
                    <label class="form-check-label" for="remember">
                        Remember me
                    </label>
                </div>
                <div class="col-3">
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
</body>
</html>