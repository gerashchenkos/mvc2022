<div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
    <?php
    foreach ($products as $product): ?>
        <div class="col">
            <div class="card shadow-sm">
                <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                     xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail"
                     preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title>
                    <rect width="100%" height="100%" fill="#55595c"></rect>
                    <text x="50%" y="50%" fill="#eceeef" dy=".3em"><?php
                        echo $product->name; ?></text>
                </svg>

                <div class="card-body">
                    <p class="card-text float-right"><?php
                        echo $product->name; ?>
                        <!--<div class="float-right form-check">-->
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="checkbox" id="flexCheckDefault" class="form-check-input"
                               name="products[]" value="<?php
                        echo $product->id; ?>">
                        <label class="form-check-label" for="flexCheckDefault">
                            Add to Cart
                        </label>
                        <!--</div>-->
                    </p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <small class="text-muted"><?php
                                echo ucfirst($product->category->name); ?></small>
                        </div>
                        <small class="text-muted">$<?php
                            echo $product->price; ?></small>
                    </div>
                </div>

            </div>
        </div>
    <?php
    endforeach; ?>
</div>