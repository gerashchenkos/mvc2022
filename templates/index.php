<main>
    <section class="py-5 text-center container">
        <div class="row py-lg-5">
            <div class="col-lg-6 col-md-8 mx-auto">
                <h1 class="fw-light">Album example</h1>
                <p class="lead text-muted">Something short and leading about the collection below—its contents, the
                    creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it
                    entirely.</p>
                <p>
                    <a href="#" class="btn btn-primary my-2">Main call to action</a>
                    <a href="#" class="btn btn-secondary my-2">Secondary action</a>
                </p>
            </div>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form method="POST" onsubmit="event.preventDefault();" action="/products/search">
                        <div class="row justify-content-right">
                            <div class="col-10">
                                <input type="text" class="form-control" id="search" placeholder="<?php
                                echo $searchText ?? 'Enter Product Name'; ?>" value="" name="search" required>
                            </div>
                            <div class="col-2">
                                <button id="search-button" class="btn btn-primary">Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <br> <hr>
            <form method="POST" id="form-products" action="/products/add">
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
                    <?php
                    foreach ($products as $product): ?>
                        <div class="col">
                            <div class="card shadow-sm">
                                <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                                     xmlns="http://www.w3.org/2000/svg" role="img" aria-label="Placeholder: Thumbnail"
                                     preserveAspectRatio="xMidYMid slice" focusable="false"><title>Placeholder</title>
                                    <rect width="100%" height="100%" fill="#55595c"></rect>
                                    <text x="50%" y="50%" fill="#eceeef" dy=".3em"><?php
                                        echo $product->name; ?></text>
                                </svg>

                                <div class="card-body">
                                    <p class="card-text float-right"><?php
                                        echo $product->name; ?>
                                        <!--<div class="float-right form-check">-->
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" id="flexCheckDefault" class="form-check-input"
                                               name="products[]" value="<?php
                                        echo $product->id; ?>">
                                        <label class="form-check-label" for="flexCheckDefault">
                                            Add to Cart
                                        </label>
                                        <!--</div>-->
                                    </p>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            <small class="text-muted"><?php
                                                echo ucfirst($product->category->name); ?></small>
                                        </div>
                                        <small class="text-muted">$<?php
                                            echo $product->price; ?></small>
                                    </div>
                                </div>

                            </div>
                        </div>
                    <?php
                    endforeach; ?>
                </div>
                <br>
                <div class="row justify-content-center">
                    <div class="col-2">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

</main>

<script>
  document.getElementById('search-button').onclick = function () {search()}

  async function search () {
    let text = document.getElementById('search').value;
    document.getElementById('form-products').innerHTML = '';

    let search = {
      text: text,
    };

    let response = await fetch('/products/ajax-search', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json;charset=utf-8'
      },
      body: JSON.stringify(search)
    });

    let result = await response.json();
    document.getElementById('form-products').innerHTML = result.content;
  }
</script>
</body>
</html>