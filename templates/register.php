<div class="container-fluid">
    <div class="row justify-content-md-center">
        <div class="col-6">
            <form action="/user/post-register" method="post">
                <div class="container">

                    <div class="mb-3">
                        <label for="email" class="form-label">Name</label>
                        <input class="form-control <?php if(!empty($error['name'])): ?>is-invalid<?php endif; ?>" type="text" placeholder="Enter Name" name="name" value="<?php echo $_POST['name'] ?? ''; ?>" required>
                        <div class="invalid-feedback"><?php echo $error['name'];?></div>
                    </div>

                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input class="form-control <?php if(!empty($error['email'])): ?>is-invalid<?php endif; ?>" type="email" placeholder="Enter Email" name="email" value="<?php echo $_POST['email'] ?? ''; ?>" required>
                        <div class="invalid-feedback"><?php echo $error['email'];?></div>
                    </div>

                    <div class="mb-3">
                        <label for="password" class="form-label">Password</label>
                        <input type="password" class="form-control <?php if(!empty($error['password'])): ?>is-invalid<?php endif; ?>" id="password" placeholder="Enter Password" name="password"
                               required>
                        <div class="invalid-feedback"><?php echo $error['password'];?></div>
                    </div>

                    <div class="mb-3">
                        <label for="confirm-password" class="form-label">Confirm Password</label>
                        <input type="password" class="form-control <?php if(!empty($error['confirm_password'])): ?>is-invalid<?php endif; ?>" id="confirm-password" placeholder="Enter Password" name="confirm_password"
                               required>
                        <div class="invalid-feedback"><?php echo $error['confirm_password'];?></div>
                    </div>

                    <div class="mb-3">
                        <label for="phone" class="form-label">Phone</label>
                        <input class="form-control <?php if(!empty($error['phone'])): ?>is-invalid<?php endif; ?>" type="text" placeholder="Enter Phone" name="phone" value="<?php echo $_POST['phone'] ?? ''; ?>">
                        <div class="invalid-feedback"><?php echo $error['phone'];?></div>
                    </div>

                    <div class="col-3">
                        <button type="submit" class="btn btn-primary">Register</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>