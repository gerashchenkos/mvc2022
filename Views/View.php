<?php

namespace Shop\Views;

class View
{
    private const HEADER_NAME = "header";
    private const TEMPLATES_EXTENSION = ".php";
    private static $templatePath;
    private static $viewTemplate;

    public static function setTemplatePath()
    {
        self::$templatePath = dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR;
    }

    public static function render(string $template, array $data = [])
    {
        self::$viewTemplate = $template;
        self::setTemplatePath();
        if (!empty(self::HEADER_NAME)) {
            require_once self::$templatePath . self::HEADER_NAME . self::TEMPLATES_EXTENSION;
        }
        if (file_exists(self::$templatePath . self::$viewTemplate . self::TEMPLATES_EXTENSION)) {
            extract($data);
            require_once self::$templatePath . self::$viewTemplate . self::TEMPLATES_EXTENSION;
        } else {
            throw new \Exception('Template name is not valid');
        }
    }

    public static function renderPartial(string $template, array $data = []) : string
    {
        ob_start();
        self::$viewTemplate = $template;
        self::setTemplatePath();
        if (file_exists(self::$templatePath . self::$viewTemplate . self::TEMPLATES_EXTENSION)) {
            extract($data);
            require_once self::$templatePath . self::$viewTemplate . self::TEMPLATES_EXTENSION;
        } else {
            throw new \Exception('Template name is not valid');
        }
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

}