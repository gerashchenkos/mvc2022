<?php

namespace Shop\Routes;

class Route
{
    private static $routes = [];
    public static $currentRoute;

    public static function init()
    {
        self::$routes = [
            ["url" => "user/logout", "controller" => "User", "action" => "logout", "is_auth" => 1],
            ["url" => "user/login", "controller" => "User", "action" => "login", "is_auth" => 0],
            ["url" => "user/register", "controller" => "User", "action" => "register", "is_auth" => 0],
            ["url" => "/", "controller" => "Products", "action" => "index", "is_auth" => 1],
            ["url" => "user/post-login", "controller" => "User", "action" => "postLogin", "is_auth" => 0],
            ["url" => "user/post-register", "controller" => "User", "action" => "postRegister", "is_auth" => 0],
            ["url" => "products/add", "controller" => "Products", "action" => "add", "is_auth" => 1],
            ["url" => "cart", "controller" => "Cart", "action" => "index", "is_auth" => 1],
            ["url" => "products/search", "controller" => "Products", "action" => "search", "is_auth" => 1],
            ["url" => "products/ajax-search", "controller" => "Products", "action" => "ajaxSearch", "is_auth" => 1],
        ];
    }

    public static function run(string $url)
    {
        $route = array_filter(self::$routes, function($elem) use ($url) {
            return $elem['url'] === $url;
        });
        if (empty($route)) {
            http_response_code(404);
            die();
        }
        $route = array_values($route)[0];
        self::$currentRoute = $route;
        $class = 'Shop\Controllers\\' . $route['controller'] . 'Controller';
        $controller = new $class();
        $controller->{$route['action']}();
    }
}