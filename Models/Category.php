<?php


namespace Shop\Models;


class Category
{
    public string $id;
    public string $name;
    private static $table = 'categories';

    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public static function getById(string $id): Category | bool
    {
        $stmt = Db::getInstance()->getConnection()->prepare("SELECT * FROM `" . self::$table . "` WHERE id = :id");
        $stmt->execute(["id" => $id]);
        $category = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!empty($category)) {
            return new Category($category['id'], $category['name']);
        }
        return false;
    }
}