<?php

namespace Shop\Models;

use Shop\Models\Interfaces\UserPassword;

abstract class AbstractUser implements UserPassword
{
    public int $id;
    public string $name;
    public string $email;
    protected string $password;
    public string $phone;
    protected string $status;

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    abstract protected function encryptPassword(string $password);

    abstract public function setStatus(string $status);
}