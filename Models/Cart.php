<?php


namespace Shop\Models;


class Cart
{
    public string $id;
    public string $userId;
    public string $date;
    private static $table = 'cart';

    public function __construct(string $id, string $userId, string $date = null)
    {
        $this->id = $id;
        $this->userId = $userId;
        if (!empty($date)) {
            $this->date = $date;
        }
    }

    public static function create(string $userId): Cart
    {
        $stmt = Db::getInstance()->getConnection()->prepare(
            "INSERT INTO `cart`(`user_id`) VALUES(:user_id)"
        );
        $stmt->execute(["user_id" => $userId]);
        $id = Db::getInstance()->getConnection()->lastInsertId();
        return self::getById($id);
    }

    public static function getById(string $id): Cart|bool
    {
        $stmt = Db::getInstance()->getConnection()->prepare("SELECT * FROM `" . self::$table . "` WHERE id = :id");
        $stmt->execute(["id" => $id]);
        $cart = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!empty($cart)) {
            return new Cart($cart['id'], $cart['user_id'], $cart['date']);
        }
        return false;
    }
}