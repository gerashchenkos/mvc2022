<?php


namespace Shop\Models;

use Shop\Models\Category;

class Product
{
    public string $id;
    public string $name;
    public float $price;
    public int $amount;
    public string $categoryId;
    public Category $category;
    private static $table = 'products';

    public function __construct(string $id, string $name, float $price, int $amount, Category $category)
    {
        $this->id = $id;
        $this->name = $name;
        $this->price = $price;
        $this->amount = $amount;
        $this->category = $category;
        $this->categoryId = $category->id;
    }

    public static function getAll(): array
    {
        $stmt = Db::getInstance()->getConnection()->prepare("SELECT * FROM `" . self::$table . "`");
        $stmt->execute();
        $productsArray = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $products = [];
        foreach ($productsArray as $product) {
            $category = Category::getById($product['category_id']);
            $products[] = new Product(
                $product['id'],
                $product['name'],
                $product['price'],
                $product['amount'] ?? 0,
                $category
            );
        }
        return $products;
    }

    public static function getById(string $id): Product|bool
    {
        $stmt = Db::getInstance()->getConnection()->prepare("SELECT * FROM `" . self::$table . "` WHERE id = :id");
        $stmt->execute(["id" => $id]);
        $product = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!empty($product)) {
            $category = Category::getById($product['category_id']);
            return new Product(
                $product['id'],
                $product['name'],
                $product['price'],
                $product['amount'] ?? 0,
                $category
            );
        }
        return false;
    }

    public static function search($text): array
    {
        $stmt = Db::getInstance()->getConnection()->prepare("
            SELECT * FROM `" . self::$table . "` WHERE `name` LIKE CONCAT('%', :text, '%')");
        $stmt->execute(["text" => $text]);
        $productsArray = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $products = [];
        foreach ($productsArray as $product) {
            $category = Category::getById($product['category_id']);
            $products[] = new Product(
                $product['id'],
                $product['name'],
                $product['price'],
                $product['amount'] ?? 0,
                $category
            );
        }
        return $products;
    }

}