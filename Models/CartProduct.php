<?php


namespace Shop\Models;


class CartProduct extends Product
{
    public string $cartId;
    public int $quantity;
    private static $table = 'cart_products';

    public function __construct(
        string $id,
        string $name,
        float $price,
        int $amount,
        Category $category,
        string $cartId,
        int $quantity
    ) {
        parent::__construct($id, $name, $price, $amount, $category);
        $this->cartId = $cartId;
        $this->quantity = $quantity;
    }

    public static function getByCartId(string $cartId): array
    {
        $stmt = Db::getInstance()->getConnection()->prepare(
            "
            SELECT * FROM `" . self::$table . "` WHERE cart_id = :cart_id"
        );
        $stmt->execute(["cart_id" => $cartId]);
        $cartProducts = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $products = [];
        if (!empty($cartProducts)) {
            foreach ($cartProducts as $product) {
                //create CartProducts object
                $productObj = Product::getById($product['product_id']);
                $products[] = new CartProduct(
                    $productObj->id,
                    $productObj->name,
                    $productObj->price,
                    $productObj->amount,
                    $productObj->category,
                    $cartId,
                    $product['quantity']
                );
            }
            return $products;
        }
        return [];
    }

    public static function add(string $cartId, Product $product, int $quantity = 1): CartProduct
    {
        $cartProduct = new CartProduct(
            $product->id,
            $product->name,
            $product->price,
            $product->amount,
            $product->category,
            $cartId,
            $quantity
        );
        $stmt = Db::getInstance()->getConnection()->prepare(
            "INSERT INTO `" . self::$table . "` (`cart_id`, `product_id`, `quantity`) 
                    VALUES(:cart_id, :product_id, :quantity)"
        );
        $stmt->execute([
            "cart_id" => $cartId,
            "product_id" => $cartProduct->id,
            "quantity" => $quantity
        ]);
        return $cartProduct;
    }

    public function updateQuantity(int $quantity)
    {
        $this->quantity = $quantity;
        $stmt = Db::getInstance()->getConnection()->prepare("
            UPDATE `" . self::$table . "` SET `quantity` = :quantity WHERE cart_id = :cart_id AND product_id = :product_id");
        $stmt->execute([
           "cart_id" => $this->cartId,
           "product_id" => $this->id,
           "quantity" => $quantity
       ]);
    }
}