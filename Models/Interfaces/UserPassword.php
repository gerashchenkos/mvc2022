<?php

namespace Shop\Models\Interfaces;

interface UserPassword
{
    public function getPassword();

    public function setPassword(string $password);
}