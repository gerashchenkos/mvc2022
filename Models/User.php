<?php

namespace Shop\Models;

class User extends AbstractUser
{
    private const SALT = "edje&x$11_cVB";
    private const STATUSES = ['not activated', 'active', 'disabled'];

    public function __construct(
        int $id,
        string $name,
        string $email,
        string $password,
        string $phone = null,
        string $status = 'not activated'
    ) {
        $this->id = (int)$id;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        if (!empty($phone)) {
            $this->phone = $phone;
        }
        $this->setStatus($status);
    }

    public function setPassword(string $password): void
    {
        $this->password = $this->encryptPassword($password);
    }

    protected function encryptPassword(string $password): string
    {
        return crypt($password, self::SALT);
    }


    public function setStatus(string $status): void
    {
        if (in_array($status, self::STATUSES)) {
            $this->status = $status;
            $stmt = Db::getInstance()->getConnection()->prepare("UPDATE `users` SET `status` = :status WHERE id = :id");
            $stmt->execute(["status" => $status, "id" => $this->id]);
        } else {
            throw new Exception('Status is invalid');
        }
    }

    public function __destruct()
    {
        //var_dump("class is destroyed");
    }

    public function __call($name, $arguments)
    {
        // Замечание: значение $name регистрозависимо.
        $nameParams = explode("get", $name);
        if (in_array(strtolower($nameParams[1]), ["id", "name", "email", "phone"])) {
            return $this->{strtolower($nameParams[1])};
        }
    }

    public function __get(string $name)
    {
        if (in_array($name, ["status"])) {
            return $this->status;
        }
    }

    public function __set(string $name, mixed $value): void
    {

    }

    public function __toString(): string
    {
        return $this->id . " " . $this->name . " " . $this->email;
    }

    public function __serialize(): array
    {
        return ["id" => $this->id, "name" => $this->name, "email" => $this->email];
    }

    public static function create(array $userData): User
    {
        $stmt = Db::getInstance()->getConnection()->prepare("INSERT INTO `users`(`name`, `email`, `password`, `phone`) VALUES(:name, :email, :password, :phone)");
        $user = [
            "name" => $userData['name'],
            "email" => $userData['email'],
            "password" => $userData['password'],
            "phone" => $userData['phone'],
        ];
        $stmt->execute($user);
        $id = Db::getInstance()->getConnection()->lastInsertId();
        $user = new User($id, $userData['name'], $userData['email'], $userData['password'], $userData['phone']);
        return $user;
    }

    public static function login(string $email, string $password): User|bool
    {
        $stmt = Db::getInstance()->getConnection()->prepare("SELECT * FROM users WHERE `email` = :email and `password` = :password");
        $stmt->execute(["email" => $email, "password" => $password]);
        $user = $stmt->fetch(\PDO::FETCH_ASSOC);
        if (!empty($user)) {
            $user = new User($user['id'], $user['name'], $user['email'], $user['password'], $user['phone'] ?? null, $user['status']);
            return $user;
        }
        return false;
    }

}