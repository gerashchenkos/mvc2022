<?php


namespace Shop\Controllers;

use Shop\Controllers\Controller;
use Shop\Models\CartProduct;
use Shop\Views\View;
use Shop\Models\Product;
use Shop\Models\Cart;

class ProductsController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->data['products'] = Product::getAll();
        View::render('index', $this->data);
    }

    public function add()
    {
        if (!empty($_POST['products'])) {
            if (empty($_SESSION['cart_id'])) {
                $cart = Cart::create(unserialize($_SESSION['user'])->id);
                $_SESSION['cart_id'] = $cart->id;
            } else {
                $cart = Cart::getById($_SESSION['cart_id']);
            }
            $products = CartProduct::getByCartId($cart->id);
            foreach($_POST['products'] as $product) {
                $isExist = array_filter($products, function($v) use ($product) {
                    return $v->id == $product;
                });
                if (empty($isExist)) {
                    CartProduct::add($cart->id, Product::getById($product));
                } else {
                    $cartProduct = current($isExist);
                    $cartProduct->updateQuantity($cartProduct->quantity + 1);
                }
            }
            header("Location: /cart");
            die();
        }
    }

    public function search()
    {
        $this->data['products'] = Product::search($_POST['search']);
        $this->data['searchText'] = $_POST['search'];
        View::render('index', $this->data);
    }

    public function ajaxSearch()
    {
        $body = file_get_contents('php://input');
        $data = json_decode($body, true);
        $this->data['products'] = Product::search($data['text']);
        $this->data['searchText'] = $data['text'];
        $content = View::renderPartial('products_partial', $this->data);
        header('Content-Type: application/json; charset=utf-8');
        echo json_encode(["content" => $content]);
    }
}