<?php


namespace Shop\Controllers;

use Shop\Models\CartProduct;
use Shop\Views\View;

class CartController
{
    public function index()
    {
        if (empty($_SESSION['cart_id'])) {
            header("Location: /");
            die();
        }
        $this->data['products'] = CartProduct::getByCartId($_SESSION['cart_id']);
        View::render('cart', $this->data);
    }

}