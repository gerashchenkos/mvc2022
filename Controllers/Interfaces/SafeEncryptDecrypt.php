<?php

namespace Shop\Controllers\Interfaces;

interface SafeEncryptDecrypt
{
    public function safeEncrypt(string $message, string $key): string;

    public function safeDecrypt(string $encrypted, string $key): string;
}