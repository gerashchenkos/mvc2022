<?php

namespace Shop\Controllers;

use Shop\Views\View;
use Shop\Models\User;
use Shop\Models\Db;
use Shop\Routes\Route;
use Shop\Controllers\Controller;

class UserController extends Controller
{
    private $exceptions = ['logout'];

    public function __construct()
    {
        parent::__construct();
    }

    public function login()
    {
        View::render('login');
    }

    public function postLogin()
    {
        if (!empty($_POST['email']) && !empty($_POST['psw'])) {
            $user = User::login($_POST['email'], $_POST['psw']);
            if (!empty($user)) {
                $_SESSION['user'] = serialize($user);
                if (!empty($_POST['remember'])) {
                    setcookie("id", $this->safeEncrypt($user->id, $_ENV['ENCRYPT_KEY']), time() + (3600 * 24 * 60));
                }
                header("Location: /");
            } else {
                $this->data['email'] = $_POST['email'];
                $this->data['isError'] = 1;
                View::render('login', $this->data);
            }
        }

    }

    public function logout()
    {
        session_destroy();
        unset($_SESSION['user']);
        if (isset($_SERVER['HTTP_COOKIE'])) {
            $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
            foreach($cookies as $cookie) {
                $parts = explode('=', $cookie);
                $name = trim($parts[0]);
                setcookie($name, '', time()-1000);
                setcookie($name, '', time()-1000, '/');
            }
        }
        //setcookie('id', '', -1, '/');
        header("Location: /user/login");
    }

    public function register()
    {
        View::render('register');
    }

    public function postRegister()
    {
        $this->data['error'] = [];
        $notEmpty = ['name', 'email', 'password', 'confirm_password'];
        if (!empty($_POST)) {
            foreach ($_POST as $key => $val) {
                if (in_array($key, $notEmpty) && empty($val)) {
                    $this->data['error'][$key] = ucfirst($key) . ' is required';
                }

                if ($key === "email") {
                    if (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
                        $this->data['error'][$key] = ucfirst($key) . ' is invalid';
                        continue;
                    }

                    $stmt = Db::getInstance()->getConnection()->prepare("SELECT id FROM users WHERE `email` = :email");
                    $stmt->execute(["email" => $val]);
                    if (!empty($stmt->fetchColumn())) {
                        $this->data['error'][$key] = ucfirst($key) . " " . $val . ' is already used';
                    }
                }

                if ($key === "phone" && !empty($val)) {
                    if (!preg_match("/^[0-9]{3}-[0-9]{4}-[0-9]{4}$/", $val)) {
                        $this->data['error'][$key] = ucfirst($key) . ' is invalid';
                    }
                }
            }

            if ($_POST['password'] !== $_POST['confirm_password']) {
                $this->data['error']['password'] = 'Password was not confirmed';
            }

            if (empty($this->data['error'])) {
                $user = User::create($_POST);
                $_SESSION['user'] = serialize($user);
                header("Location: /");
                die();
            }
            View::render('register', $this->data);
        }
    }
}