<?php


namespace Shop\Controllers;

use Shop\Controllers\Interfaces\SafeEncryptDecrypt;
use Shop\Models\Db;
use Shop\Models\User;
use Shop\Routes\Route;

class Controller implements SafeEncryptDecrypt
{
    protected array $data = [];

    public function __construct()
    {
        $user = $this->checkRememberMe($_SESSION['user'] ?? [], $_COOKIE['id'] ?? 0);
        if ($user) {
            $_SESSION['user'] = serialize($user);
        }
        if (Route::$currentRoute['is_auth'] === 1 && empty($_SESSION['user'])) {
            header("Location: /user/login");
            die();
        } elseif (Route::$currentRoute['is_auth'] === 0 && !empty($_SESSION['user'])) {
            header("Location: /");
            die();
        }
    }

    private function checkRememberMe(string|array $userSession, string|int $id): User|bool
    {
        if (empty($userSession) && !empty($id)) {
            $id = (int)$this->safeDecrypt($id, $_ENV['ENCRYPT_KEY']);
            $stmt = Db::getInstance()->getConnection()->prepare("SELECT * FROM `users` WHERE `id` = :id");
            $stmt->execute(["id" => $id]);
            $user = $stmt->fetch(\PDO::FETCH_ASSOC);
            return new User($user['id'], $user['name'], $user['email'], $user['password'], $user['phone'] ?? null, $user['status']);
        }
        return false;
    }

    /**
     * Encrypt a message
     *
     * @param string $message - message to encrypt
     * @param string $key - encryption key
     * @return string
     * @throws RangeException
     */
    public function safeEncrypt(string $message, string $key): string
    {
        if (mb_strlen($key, '8bit') !== SODIUM_CRYPTO_SECRETBOX_KEYBYTES) {
            throw new RangeException('Key is not the correct size (must be 32 bytes).');
        }
        $nonce = random_bytes(SODIUM_CRYPTO_SECRETBOX_NONCEBYTES);

        $cipher = base64_encode(
            $nonce.
            sodium_crypto_secretbox(
                $message,
                $nonce,
                $key
            )
        );
        sodium_memzero($message);
        sodium_memzero($key);
        return $cipher;
    }

    /**
     * Decrypt a message
     *
     * @param string $encrypted - message encrypted with safeEncrypt()
     * @param string $key - encryption key
     * @return string
     * @throws Exception
     */
    public function safeDecrypt(string $encrypted, string $key): string
    {
        $decoded = base64_decode($encrypted);
        $nonce = mb_substr($decoded, 0, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, '8bit');
        $ciphertext = mb_substr($decoded, SODIUM_CRYPTO_SECRETBOX_NONCEBYTES, null, '8bit');

        $plain = sodium_crypto_secretbox_open(
            $ciphertext,
            $nonce,
            $key
        );
        if (!is_string($plain)) {
            throw new Exception('You are bad hacker!');
        }
        sodium_memzero($ciphertext);
        sodium_memzero($key);
        return $plain;
    }
}