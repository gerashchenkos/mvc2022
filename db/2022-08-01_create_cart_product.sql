ALTER TABLE `cart` DROP COLUMN `product_id`;
ALTER TABLE `cart` DROP COLUMN `quantity`;

CREATE TABLE `cart_products` (
    `cart_id` bigint unsigned NOT NULL,
    `product_id` bigint unsigned NOT NULL,
    `quantity` bigint NOT NULL,
    PRIMARY KEY (`cart_id`, `product_id`)
);